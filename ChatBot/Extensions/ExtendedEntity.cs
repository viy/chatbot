﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatBot.Extensions
{
    public class ExtendedEntity<TEntity> where TEntity : class
    {
        public ExtendedEntity(List<TEntity> entities)
        {
            Entities = entities;
            Count = entities.Count();
        }

        public  IEnumerable<TEntity> Entities { get; set; }

        public int Count { get; set; }
    }
}

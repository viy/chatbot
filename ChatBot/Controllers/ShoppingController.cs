﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ChatBot.Enums;
using ChatBot.Extensions;
using ChatBot.Models;
using ChatBot.Services;
using CsvHelper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;

namespace ChatBot.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ShoppingController : ControllerBase
    {
        public ShoppingController()
        {
            Provider = new CsvProvider<Shopping>();
        }

        private CsvProvider<Shopping> Provider { get; }

        [HttpGet]
        public ExtendedEntity<Shopping> Get() => new ExtendedEntity<Shopping>(Provider.GetList().ToList());

        [HttpGet, Route("{category}/by-category")]
        [SwaggerOperation("0 - antiques, 1 - department_store, 2 - fashion, 3 - furniture_decor, 4 - gift, 5 - toy")]
        public ExtendedEntity<Shopping> GetByCategory([FromRoute]ShoppingCategory category)
        {
            var nightLifeCategory = $"shopping:{category.ToString()}";
            var result = Provider.GetList()
                .Where(x => x.Categories1 == nightLifeCategory || x.Categories2 == nightLifeCategory || x.Categories3 == nightLifeCategory || x.Categories4 == nightLifeCategory)
                .OrderBy(x => x.Rating);
            return new ExtendedEntity<Shopping>(result.ToList());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ChatBot.Enums;
using ChatBot.Extensions;
using ChatBot.Models;
using ChatBot.Services;
using CsvHelper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;

namespace ChatBot.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FoodController : ControllerBase
    {
        public FoodController()
        {
            Provider = new CsvProvider<Food>();
        }

        private CsvProvider<Food> Provider { get; }

        [HttpGet]
        public ExtendedEntity<Food> Get() => new ExtendedEntity<Food>(Provider.GetList().ToList());

        [HttpGet, Route("{category}/by-category")]
        [SwaggerOperation("0 - american, 1 - asian, 2 - cafes_coffee, 3 - fast_food, " +
            "4 - french, 5 - indian, 6 - international, 7 - italian, 8 - meat, 9 - mexican, 10 - pizza, " +
            "11 - seafood, 12 - steakhouse, 13 - sushi, 14 - thai, 15 - vegetarian, 16 - wine_bar")]
        public ExtendedEntity<Food> GetByCategory([FromRoute]FoodCategory category)
        {
            var foodCategory = $"food:{category.ToString()}";
            var result = Provider.GetList()
                .Where(x => x.Categories1 == foodCategory || x.Categories2 == foodCategory || x.Categories3 == foodCategory || x.Categories4 == foodCategory)
                .OrderBy(x => x.Rating);
            return new ExtendedEntity<Food>(result.ToList());
        }
    }
}

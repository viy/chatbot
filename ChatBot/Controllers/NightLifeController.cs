﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ChatBot.Enums;
using ChatBot.Extensions;
using ChatBot.Models;
using ChatBot.Services;
using CsvHelper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;

namespace ChatBot.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NightLifeController : ControllerBase
    {
        public NightLifeController()
        {
            Provider = new CsvProvider<NightLife>();
        }

        private CsvProvider<NightLife> Provider { get; }

        [HttpGet]
        public ExtendedEntity<NightLife> Get() => new ExtendedEntity<NightLife>(Provider.GetList().ToList());

        [HttpGet, Route("{category}/by-category")]
        [SwaggerOperation("0 - bar, 1 - jazz_cafe, 2 - lounge, 3 - night_club, 4 - american, 5 - diner, 6 - fast_food, 7 - international, 8 - sushi, 9 - wine_bar")]
        public ExtendedEntity<NightLife> GetByCategory([FromRoute]NightLifeCategory category)
        {
            var nightLifeCategory = GetCategory(category);
            var result = Provider.GetList()
                .Where(x => x.Categories1 == nightLifeCategory || x.Categories2 == nightLifeCategory || x.Categories3 == nightLifeCategory || x.Categories4 == nightLifeCategory)
                .OrderBy(x => x.Rating);
            return new ExtendedEntity<NightLife>(result.ToList());
        }

        private static string GetCategory(NightLifeCategory category)
        {
            var nightLifeCategory = category.ToString();

            switch (nightLifeCategory)
            {
                case "bar":
                case "jazz_cafe":
                case "lounge":
                case "night_club":
                    return $"nightlife:{nightLifeCategory}";
                case "american":
                case "diner":
                case "fast_food":
                case "international":
                case "sushi":
                case "wine_bar":
                    return $"food:{nightLifeCategory}";
                default:
                    return string.Empty;
            }
        }

    }
}

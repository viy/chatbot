﻿using ChatBot.Models;
using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ChatBot.Services
{
    public class CsvProvider<T> where T: class
    {
        private TextReader readerFood = new StreamReader("Sources\\sf_food.csv");
        private TextReader readerNightLife = new StreamReader("Sources\\sf_nightlife.csv");
        private TextReader readerNonFood = new StreamReader("Sources\\sf_nonfood_places.csv");
        private TextReader readerShopping = new StreamReader("Sources\\sf_shopping.csv");

        public IEnumerable<T> GetList()
        {
            var csvReader = new CsvReader(GetReader<T>(), CultureInfo.InvariantCulture);
            return csvReader.GetRecords<T>();
        }

        private TextReader GetReader<T>()
        {
            switch (typeof(T).Name)
            {
                case "Food":
                    return readerFood;
                case "NightLife":
                    return readerNightLife;
                case "NonFood":
                    return readerNonFood;
                case "Shopping":
                    return readerShopping;
                default:
                    throw new Exception("Reader not defined.");
            }
        }
    }
}
